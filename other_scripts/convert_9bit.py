def convert_to_ascii(binary_sequence):
    # Split the string by spaces to get a list of two-digit numbers
    numbers = binary_sequence.split()

    # Group every 7 numbers together
    groups = [numbers[i:i+7] for i in range(0, len(numbers), 7)]
    print(groups)
    ascii_characters = []

    for group in groups:
        # Convert each number to binary and then to ASCII individually
        for num in group:
            binary_string = format(int(num), '07b')
            print(binary_string)
            ascii_character = chr(int(binary_string, 2))
            ascii_characters.append(ascii_character)

    return ascii_characters

binary_sequence = "83 80 91 77 86 84 85 70 79 74 78 33 91 81 83 66 87 90 33 75 84 74 33 81 83 80 76 66 91 66 77 33 91 66 75 74 78 66 87 70 33 81 83 80 72 83 66 78 66 85 80 83 84 76 70 33 69 80 87 70 69 79 80 84 85 74 47 33 85 80 33 75 70 33 87 84 66 76 33 85 70 81 83 87 70 33 91 66 68 66 85 70 76 34 33 69 66 77 84 74 33 86 76 80 77 90 33 79 66 75 69 70 84 33 79 66 33 66 69 83 70 84 70 33 81 66 83 69 86 67 74 68 76 90 46 73 66 68 76 70 83 47 69 70 77 85 66 46 84 76 80 77 66 47 68 91 45 33 91 66 77 80 91 33 84 74 33 91 69 70 33 86 68 70 85 33 66 33 81 86 84 85 33 84 70 33 69 80 33 75 70 75 74 68 73 33 83 70 84 70 79 74 47 33 79 66 33 68 66 84 70 33 91 66 77 70 91 74 34 34 34 33 52 49 33 79 70 75 77 70 81 84 74 68 73 33 81 80 91 87 70 78 70 33 69 80 33 71 74 79 66 77 70"
# ascii_characters = convert_to_ascii(binary_sequence)
# ascii_string = ''.join(ascii_characters)

# print(ascii_string)

# Split the string by spaces to get a list of numbers
numbers = '83'.split()

# Convert each number to binary and join them together
binary_string = ''.join([format(int(num), '07b') for num in numbers])

print(binary_string)

def convert_binary_to_ascii(binary_string):
    # Check if the binary string is a multiple of 8 bits
    if len(binary_string) % 8 != 0:
        raise ValueError("Binary string must be a multiple of 8 bits")

    # Convert the binary string to a list of 8-bit binary strings
    eight_bit_binary_strings = []
    for i in range(0, len(binary_string), 8):
        eight_bit_binary_strings.append(binary_string[i:i+8])

    # Convert each 8-bit binary string to an ASCII character
    ascii_characters = []
    for eight_bit_binary_string in eight_bit_binary_strings:
        ascii_character = int(eight_bit_binary_string, 2)
        ascii_characters.append(chr(ascii_character))

    # Return the ASCII string
    return "".join(ascii_characters)

# Example usage
# binary_string = "101001110100001011011100110110101101010100"
try:
    print(binary_string)
    ascii_string = convert_binary_to_ascii(binary_string)
    print(ascii_characters)
except ValueError as e:
    print(e)