def convert_to_bcd(binary_sequence):
    # Split the string by spaces to get a list of two-digit numbers
    numbers = binary_sequence.split()

    # Convert each number to binary and concatenate them to form a large binary number
    binary_string = ''.join([format(int(num), '07b') for num in numbers])

    # Split the large binary number into 4-bit chunks
    chunks = [binary_string[i:i+4] for i in range(0, len(binary_string), 4)]

    # Convert each 4-bit chunk to a decimal number
    decimal_numbers = [int(chunk, 2) for chunk in chunks]

    # Add 65 to each number and convert it to ASCII
    ascii_characters = [chr(num + 65) for num in decimal_numbers]

    return ascii_characters

binary_sequence = "83 80 91 77 86 84 85 70 79 74 78 33 91 81 83 66 87 90 33 75 84 74 33 81 83 80 76 66 91 66 77 33 91 66 75 74 78 66 87 70 33 81 83 80 72 83 66 78 66 85 80 83 84 76 70 33 69 80 87 70 69 79 80 84 85 74 47 33 85 80 33 75 70 33 87 84 66 76 33 85 70 81 83 87 70 33 91 66 68 66 85 70 76 34 33 69 66 77 84 74 33 86 76 80 77 90 33 79 66 75 69 70 84 33 79 66 33 66 69 83 70 84 70 33 81 66 83 69 86 67 74 68 76 90 46 73 66 68 76 70 83 47 69 70 77 85 66 46 84 76 80 77 66 47 68 91 45 33 91 66 77 80 91 33 84 74 33 91 69 70 33 86 68 70 85 33 66 33 81 86 84 85 33 84 70 33 69 80 33 75 70 75 74 68 73 33 83 70 84 70 79 74 47 33 79 66 33 68 66 84 70 33 91 66 77 70 91 74 34 34 34 33 52 49 33 79 70 75 77 70 81 84 74 68 73 33 81 80 91 87 70 78 70 33 69 80 33 71 74 79 66 77 70"
ascii_characters = convert_to_bcd(binary_sequence)
print(''.join(ascii_characters))